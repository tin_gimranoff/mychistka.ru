from django.core.mail import send_mail
from django.http import HttpResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt

from mainpage.models import Settings
from mainpage.forms import PhoneForm, CallbackForm
from mychistka.settings import FROM_EMAIL


def index(request):
    settings = Settings.objects.first()
    phone_form = PhoneForm
    callback_form = CallbackForm
    return render(request, 'mainpage.html', {
        'settings': settings,
        'phone_form': phone_form,
        'callback_form': callback_form
    })

@csrf_exempt
def send_form_phone(request):
    settings = Settings.objects.first()
    if request.method == 'POST':
        form = PhoneForm(request.POST)
        if form.is_valid():
            phone = form.cleaned_data['phone']
            message = "Новый запрос на обратный завонок от %s" % phone
            send_mail('Новый запрос на обратный завонок с сайта mychistka.ru', message, FROM_EMAIL,
                      [settings.email_for_form_phone])
            return HttpResponse("OK")


@csrf_exempt
def send_callback_form(request):
    settings = Settings.objects.first()
    if request.method == 'POST':
        form = CallbackForm(request.POST)
        if form.is_valid():
            email = form.cleaned_data['email']
            _message = form.cleaned_data['message']
            message = "Новый запрос из формы от %s\nСообщение: %s" % (email, _message)
            send_mail('Новый запрос из формы с сайта mychistka.ru', message, FROM_EMAIL,
                      [settings.email_for_form_requirements])
            return HttpResponse("OK")
