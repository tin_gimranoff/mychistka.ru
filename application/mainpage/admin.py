from django.contrib import admin
from mainpage.models import Settings

class SettingsAdmin(admin.ModelAdmin):

    fieldsets = (
        ('Meta теги', {
           'fields': ('meta_title', 'meta_keywords', 'meta_description')
        }),
        ('Emails', {
           'fields': ('email_for_form_phone', 'email_for_form_requirements'),
        }),
        ('Настройки сайта', {
            'fields': ('address_in_header', 'phone1', 'phone2', 'price_list', 'discount', 'address_in_footer'),
        }),
    )

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


admin.site.register(Settings, SettingsAdmin)
