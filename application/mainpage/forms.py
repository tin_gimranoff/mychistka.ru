from django import forms


class PhoneForm(forms.Form):
    phone = forms.CharField(widget=forms.TextInput(attrs={
        'name': 'phone',
        'placeholder': '+7 (___) ___-__-__',
    }))


class CallbackForm(forms.Form):
    email = forms.EmailField(widget=forms.EmailInput(attrs={
        'name': 'mail',
        'placeholder': 'mariota@mb-art.biz',
    }))

    message = forms.CharField(widget=forms.Textarea(attrs={
        'name': 'message',
        'rows': '4',
    }))
