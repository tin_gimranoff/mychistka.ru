# -*- coding: utf-8 -*-

from django.db import models

class Settings(models.Model):

    def __str__(self):
        return u'Настройки'

    class Meta:
        db_table = 'settings'
        verbose_name = u'Настройки'
        verbose_name_plural = u'Настройки'

    meta_title = models.CharField(max_length=255, blank=False, default='', verbose_name=u'Заголовок')
    meta_keywords = models.CharField(max_length=255, blank=False, default='', verbose_name=u'Ключевые слова')
    meta_description = models.TextField(blank=False, default='', verbose_name=u'Описание')
    email_for_form_phone = models.CharField(max_length=255, blank=False, default='', verbose_name=u'Email для писем с формы обратного звонка')
    email_for_form_requirements = models.CharField(max_length=255, blank=False, default='', verbose_name=u'Email для писем с формы c требованиями')
    address_in_header = models.TextField(max_length=255, blank=False, verbose_name=u'Адрес в шапке', default='')
    phone1 = models.CharField(max_length=20, blank=False, verbose_name=u'Номер телефона 1')
    phone2 = models.CharField(max_length=20, blank=False, verbose_name=u'Номер телефона 2')
    price_list = models.FileField(upload_to='prices/', null=False, blank=False, verbose_name=u'Прайс-лист')
    discount = models.IntegerField(default=50, verbose_name=u'Размер скидки')
    address_in_footer = models.TextField(max_length=255, blank=False, verbose_name=u'Адрес в футере', default='')
