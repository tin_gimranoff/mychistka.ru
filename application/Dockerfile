FROM python:3.5-alpine

# set work directory
WORKDIR /usr/src/app

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# add additional packages
RUN apk --update add libxml2-dev libxslt-dev libffi-dev gcc musl-dev libgcc openssl-dev curl mariadb-dev
RUN apk add jpeg-dev zlib-dev freetype-dev lcms2-dev openjpeg-dev tiff-dev tk-dev tcl-dev

# install dependencies
RUN pip install -U wheel
RUN pip install --upgrade pip
COPY ./requirements.txt /usr/src/app/requirements.txt
RUN pip install -r requirements.txt

# copy wait-for-it.sh
COPY ./entrypoint.py /usr/src/app/entrypoint.py
RUN chmod +x /usr/src/app/entrypoint.py

# copy project
COPY . /usr/src/app/

# run entrypoint.py
ENTRYPOINT ["python", "/usr/src/app/entrypoint.py"]