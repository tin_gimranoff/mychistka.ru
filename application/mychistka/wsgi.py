"""
WSGI config for mychistka project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/2.1/howto/deployment/wsgi/
"""

import os, sys

# add the hellodjango project path into the sys.path
sys.path.append('/home/mychistka/mychistka')

# add the virtualenv site-packages path to the sys.path
sys.path.append('/home/mychistka/mychistka/venv/lib/python3.5/site-packages')

from django.core.wsgi import get_wsgi_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "mychistka.settings")

application = get_wsgi_application()
