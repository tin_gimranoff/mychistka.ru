$(document).ready(function(){
	$(".main").onepage_scroll({
	   sectionContainer: "section"   
	});

	$(".arrow a").bind('click', function(){
		$(".main").moveDown();
	});

	$(".top-btn a").bind('click', function(){
		$(".main").moveTo(1);
	});

	$(".screen2-form-button").bind('click', function(e){
		e.preventDefault();
		var phone = $(".phone-form input[name=phone]").val();
		if(typeof phone == "undefined" || phone == '') {
			$(".error-validate-phone").empty().append("Вы не ввели номер телефона").show();
			$(".phone-form input[name=phone]").css({'border-color':'red'});
			return;
		}
		if(!phone.match(/^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/g)) {
			$(".error-validate-phone").empty().append("Пожалуйста введите корректный номер.").show();
			$(".phone-form input[name=phone]").css({'border-color':'red'});
			return;
		}
		$(".error-validate-phone").empty().hide();
		$(".phone-form input[name=phone]").css({'border-color':'initial'});

		$.ajax({
			url: '/ajax/send_form_phone/',
			method: 'post',
			data: {phone: phone},
			dataType: 'text',
			success: function(data) {
				if(data == 'OK') {
					$(".page2 .phone-form-label").empty().append('Мы вскоре с вами свяжемся!')
                    $(".page2 .phone-form, .page2 .phone-input-comment").remove();
                }
			}
		});
	});

	$(".screen3-form-button").bind('click', function(e){
		e.preventDefault();
		var email = $(".phone-form input[name=email]").val();
		var message = $("textarea[name=message]").val();
		if(typeof email == "undefined" || email == '') {
			$(".error-validate-email").empty().append("Вы не ввели e-mail").show();
			$(".phone-form input[name=email]").css({'border-color':'red'});
			return;
		}
		if(!email.match(/^[-._a-z0-9]+@(?:[a-z0-9][-a-z0-9]+\.)+[a-z]{2,6}$/g)) {
			$(".error-validate-email").empty().append("Пожалуйста введите корректный e-mail.").show();
			$(".phone-form input[name=email]").css({'border-color':'red'});
			return;
		}
		$(".error-validate-email").empty().hide();
		$(".phone-form input[name=email]").css({'border-color':'initial'});
		if(message == '' || message.length < 10) {
			$(".error-validate-message").empty().append("Сообщение должно быть не короче 10 символов.").show();
			$("textarea input[name=message]").css({'border-color':'red'});
			return;
		}
		$(".error-validate-message").empty().hide();
		$("textarea input[name=message]").css({'border-color':'initial'});

		$.ajax({
			url: '/ajax/send_callback_form/',
			method: 'post',
			data: {email: email, message: message},
			dataType: 'text',
			success: function(data) {
				if(data == 'OK') {
					$(".page3 .phone-form-label").empty().append('Мы вскоре с вами свяжемся!')
                    $(".page3 .phone-form, .page3 .phone-input-comment, .screen3-form-button").remove();
                }
			}
		});
	});
});